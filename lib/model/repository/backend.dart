import 'dart:convert';

import 'package:http/http.dart' as http;

class BackendRepository {
  Future<int> calculateValue(int amount, int parts, String periodicity) async {
    var request = {
      "monto": amount,
      "numeroCuotas": parts,
      "periodicidad": periodicity
    };

    var url = Uri.https("ventas-back-31269.herokuapp.com", "api/ventas/cuota");
    var response = await http.post(url,
        headers: {'Content-Type': 'application/json'},
        body: json.encode(request));

    var body = json.decode(response.body);
    if (response.statusCode != 200) {
      return Future.error(body["message"]);
    }

    return body["valor"];
  }
}
